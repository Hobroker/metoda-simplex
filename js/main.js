var test = {
        one: {
            equations: [
                [-1, -3],
                [2, 5],
                [3, 7]
            ],
            results: [-6, 18, 25],
            z: []
        }
    },
    magic = {
        equations: [],
        result: [],
        z: []
    }

$(function() {
    $('#second_step table').empty();
    $('#first_step input').change(function() {
        Generate();
    });
    $('#first_step').submit(function(e) {
        e.preventDefault();
        Generate();
    })
    $('#second_step').submit(function(e) {
        e.preventDefault();
        MakeMe();
    });
    //
    $('#first_step').submit();
    $('#second_step').submit();
    DoMe();
    // Test();
    //
})

function Generate(auto) {
    rows = $('#first_step #rows').val();
    cols = $('#first_step #cols').val();
    ATablePlease(rows, cols, auto);
}

function Test() {
    $('#first_step #rows').val(test.one.equations.length)
    $('#first_step #cols').val(test.one.equations[0].length)
    Generate(true)
}

function ATablePlease(rows, cols, auto) {
    if (auto) {}
    $('#second_step .one_sub').empty();
    $('#second_step .two_sub').empty();
    for (i = 1; i <= rows; i++) {
        row = '<tr>';
        if (i == 1) {
            row2 = '<tr><td nowrap>Z(X) = ';
        }
        for (j = 1; j <= cols; j++) {
            cell = $('.stuff .input_x').clone();
            cell.find('input').attr('value', Math.floor(Math.random() * 6) + 1)
            cell.find('sub').text(j);
            row += '<td>' + cell.html();
            if (i == 1) {
                cell.find('sub').text(j);
                row2 += '<td>' + cell.html();
            }
        }
        cell = $('.stuff .input_res').clone();
        cell.find('input').attr('value', Math.floor(Math.random() * 6) + 1)
        row += '<td>' + cell.html();
        $('#second_step .one_sub').append(row);
        if (i == 1) {
            cell = $('.stuff .arrow_select').clone();
            row2 += '<td>' + cell.html();
            $('#second_step .two_sub').append(row2);
        }
    }
}

function MakeMe() {
    magic.equations = []
    magic.result = []
    magic.z = []
    var countI = 0,
        countJ = 0;
    $('#second_step .one_sub tr').each(function() {
        magic.equations[countI] = []
        countJ = 0;
        changeSign = $('select', this).val() == '>='
        $('td', this).each(function() {
            val = parseInt($('input', this).val())
            if (changeSign)
                val = -val
            if ($('.input-group', this).hasClass('val-x')) {
                magic.equations[countI][countJ] = val
            } else
            if ($('.input-group', this).hasClass('val-res')) {
                magic.result[countI] = val
            }
            countJ++;
        });
        countI++;
    });
    countI = 0;
    changeSign = $('#second_step .val-select select').val() == 'min'
    $('#second_step .two_sub tr td').each(function(i, el) {
        if ($('.input-group', this).hasClass('val-x')) {
            val = parseInt($('input', this).val())
            if (changeSign)
                val = -val
            magic.z[countI] = val
            countI++;
        }
    });
    newLength = magic.equations[0].length + magic.equations.length
    for (i = 0; i < magic.equations.length; i++) {
        xs = i + magic.equations[i].length
        for (j = magic.equations[i].length; j < newLength; j++) {
            if (xs == j)
                magic.equations[i][j] = 1;
            else
                magic.equations[i][j] = 0;
        }
    }
    newLength = magic.equations.length + magic.z.length
    for (i = magic.z.length; i < newLength; i++) {
        magic.z[i] = 0
    }
    console.log(magic.equations);
    console.log(magic.result);
    console.log(magic.z);
}

function DoMe() {
    where = $('#result thead')
    where.empty();
    row = $('<tr>')
    row.append('<th rowspan="2" class="text-center">Baza</th>')
    row.append('<th rowspan="2" class="text-center">C<sub>i</sub></th>')
    row.append('<th rowspan="2" class="text-center">B</th>')
    for (i = 0; i < magic.z.length; i++) {
        row.append('<td>' + magic.z[i])
    }
    where.append(row)
    row = $('<tr>')
    for (i = 0; i < magic.z.length; i++) {
        row.append('<th>X<sub>' + (i + 1) + '</sub>')
    }
    where.append(row)

    where = $('#result tbody')
    where.empty();
    FirstTime()
}

function FirstTime() {
    where = $('#result tbody')
    for (i = 0; i < magic.equations.length; i++) {
        row = $('<tr>')
        row.append('<td>X<sub>' + (i + 1) + '</sub></td>')
        for (j = magic.equations[i].length; j < magic.z.length; j++) {
            
        }
        where.append(row)
    }
}

function AndAgain() {
    where = $('#result tbody')

}